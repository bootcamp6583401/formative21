package formative.twentyone.backend;



import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import formative.twentyone.backend.controllers.UserController;
import formative.twentyone.backend.models.User;
import formative.twentyone.backend.models.Zodiac;
import formative.twentyone.backend.services.UserService;
import formative.twentyone.backend.services.ZodiacService;

import java.util.List;
import java.time.LocalDate;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService service;

    @MockBean
    private ZodiacService zodService;


    @Test
    public void getAllUsers_thenReturnJsonArray() throws Exception {
        User user1 = new User();
        user1.setId(1);
        user1.setName("User1");

        User user2 = new User();
        user2.setId(2);
        user2.setName("User2");

        Zodiac zod = new Zodiac();
        zod.setId(1);
        zod.setName("Dep");

        user1.setZodiac(zod);
        user2.setZodiac(zod);


        List<User> allUsers = Arrays.asList(user1, user2);

        given(service.getAll()).willReturn(allUsers);

        mockMvc.perform(get("/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(user1.getName())))
                .andExpect(jsonPath("$[1].name", is(user2.getName())));
    }

    @Test
    public void getUserById_WhenUserExists() throws Exception {
        int userId = 1;
        User user = new User();
        user.setId(userId);
        user.setName("Name");
        user.setBirthDate(LocalDate.now());


        given(service.getById(userId)).willReturn(user);

        mockMvc.perform(get("/users/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(user.getName())))
                .andExpect(jsonPath("$.birthDate", is(user.getBirthDate().toString())))
                .andExpect(jsonPath("$.id", is(user.getId())));
    }

    @Test
    public void createUser_WhenPostUser() throws Exception {
        User user = new User();
        user.setId(1);
        user.setName("User1");

        given(service.save(any(User.class))).willReturn(user);

        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"User1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(user.getName())));
    }



    @Test
    public void deleteUser_WhenUserExists() throws Exception {
        int userId = 1;

        doNothing().when(service).deleteById(userId);

        mockMvc.perform(delete("/users/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateUser_WhenPutUser() throws Exception {
        int userId = 1;
        User user = new User();
        user.setId(userId);
        user.setName("Updated User");

        given(service.updateById(eq(userId), any(User.class))).willReturn(user);

        mockMvc.perform(put("/users/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Updated User\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(user.getName())));
    }
}