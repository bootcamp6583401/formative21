package formative.twentyone.backend;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import formative.twentyone.backend.models.User;
import formative.twentyone.backend.repositories.UserRepository;
import formative.twentyone.backend.services.impl.UserServiceImpl;



@SpringBootTest
public class UserServiceTest {
    @Mock
    private UserRepository repo;

    @InjectMocks
    private UserServiceImpl service;

    @Test
    public void whenGetAllUsers_thenReturnUserList() {
        User obj = new User();
        obj.setId(1);
        obj.setName("Test User");

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<User> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals("Test User", result.get(0).getName());
    }

    @Test
    public void whenGetUserById_thenReturnUser() {
        User obj = new User();
        obj.setId(1);
        obj.setName("Test User");

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        User result = service.getById(1);

        assertEquals("Test User", result.getName());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));
        User result = service.getById(1);
        assertNull(result);
    }

    @Test
    public void whenSaveUser_thenUserShouldBeSaved() {
        User obj = new User();
        obj.setId(1);
        obj.setName("Test User");

        when(repo.save(any(User.class))).thenReturn(obj);

        User savedUser = service.save(obj);

        assertNotNull(savedUser);
        assertEquals(obj.getName(), savedUser.getName());
        verify(repo, times(1)).save(obj);
    }



    @Test
    public void whenDeleteUser_thenUserShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdateUser_thenUserShouldBeUpdated() {
        User existingUser = new User();
        existingUser.setId(1);
        existingUser.setName("Old User Name");

        User updated = new User();
        updated.setId(1);
        updated.setName("Updated User Name");

        when(repo.findById(existingUser.getId())).thenReturn(Optional.of(existingUser));
        when(repo.save(any(User.class))).thenAnswer(i -> i.getArguments()[0]);

        User updatedUser = service.updateById(existingUser.getId(), updated);

        assertNotNull(updatedUser);
        assertEquals(updated.getName(), updatedUser.getName());
        verify(repo, times(1)).save(updatedUser);
    }

}

