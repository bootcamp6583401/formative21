package formative.twentyone.backend;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import formative.twentyone.backend.models.Zodiac;
import formative.twentyone.backend.repositories.ZodiacRepository;
import formative.twentyone.backend.services.impl.ZodiacServiceImpl;



@SpringBootTest
public class ZodiacServiceTest {
    @Mock
    private ZodiacRepository repo;

    @InjectMocks
    private ZodiacServiceImpl service;

    @Test
    public void whenGetAllZodiacs_thenReturnZodiacList() {
        LocalDate date = LocalDate.now();
        Zodiac obj = new Zodiac();
        obj.setId(1);
        obj.setName("Test Zodiac");
        obj.setStartDate(date);
        obj.setEndDate((date));

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<Zodiac> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals(date, result.get(0).getStartDate());
        assertEquals(date, result.get(0).getEndDate());
        assertEquals("Test Zodiac", result.get(0).getName());
    }

    @Test
    public void whenGetZodiacById_thenReturnZodiac() {
        Zodiac obj = new Zodiac();
        obj.setId(1);
        obj.setName("Test Zodiac");

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        Zodiac result = service.getById(1);

        assertEquals("Test Zodiac", result.getName());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));
        Zodiac result = service.getById(1);
        assertNull(result);
    }

    @Test
    public void whenSaveZodiac_thenZodiacShouldBeSaved() {
        Zodiac obj = new Zodiac();
        obj.setId(1);
        obj.setName("Test Zodiac");

        when(repo.save(any(Zodiac.class))).thenReturn(obj);

        Zodiac savedZodiac = service.save(obj);

        assertNotNull(savedZodiac);
        assertEquals(obj.getName(), savedZodiac.getName());
        verify(repo, times(1)).save(obj);
    }



    @Test
    public void whenDeleteZodiac_thenZodiacShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdateZodiac_thenZodiacShouldBeUpdated() {
        Zodiac existingZodiac = new Zodiac();
        existingZodiac.setId(1);
        existingZodiac.setName("Old Zodiac Name");

        Zodiac updated = new Zodiac();
        updated.setId(1);
        updated.setName("Updated Zodiac Name");

        when(repo.findById(existingZodiac.getId())).thenReturn(Optional.of(existingZodiac));
        when(repo.save(any(Zodiac.class))).thenAnswer(i -> i.getArguments()[0]);

        Zodiac updatedZodiac = service.updateById(existingZodiac.getId(), updated);

        assertNotNull(updatedZodiac);
        assertEquals(updated.getName(), updatedZodiac.getName());
        verify(repo, times(1)).save(updatedZodiac);
    }

}


