package formative.twentyone.backend.models;



import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity
public class Zodiac {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    @NotNull(message = "Name cannot be empty")
    private String name;

    @Getter
    @Setter
    @NotNull(message = "Start date cannot be empty")
    private LocalDate startDate;

    @Getter
    @Setter
    @NotNull(message = "End date cannot be empty")
    private LocalDate endDate;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "zodiac")
    @JsonManagedReference
    @Getter
    @Setter
    private List<User> users;

}
