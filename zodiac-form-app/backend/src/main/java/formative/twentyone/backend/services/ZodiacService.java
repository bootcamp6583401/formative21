package formative.twentyone.backend.services;



import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import formative.twentyone.backend.models.Zodiac;



public interface ZodiacService {
    List<Zodiac> getAll();

    Zodiac getById(int id);

    Zodiac save(Zodiac obj);

    void deleteById(int id);

    Zodiac updateById(int id, Zodiac brand);

    Optional<Zodiac> getOneByDates(LocalDate birthDate);


}
