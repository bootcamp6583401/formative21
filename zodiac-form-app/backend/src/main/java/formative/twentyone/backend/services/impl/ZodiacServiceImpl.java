package formative.twentyone.backend.services.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.twentyone.backend.models.Zodiac;
import formative.twentyone.backend.repositories.ZodiacRepository;
import formative.twentyone.backend.services.ZodiacService;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ZodiacServiceImpl implements ZodiacService {

    @Autowired
    ZodiacRepository repo;

    public List<Zodiac> getAll() {
        return repo.findAll();
    }

    public Zodiac save(Zodiac comment) {
        return repo.save(comment);
    }

    public Zodiac getById(int id) {
        Optional<Zodiac> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public Zodiac updateById(int id, Zodiac comment) {

        comment.setId(id);
        return repo.save(comment);

    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

    public Optional<Zodiac> getOneByDates(LocalDate birthDate)
    {
        return repo.findByDates(birthDate);
    }


}