package formative.twentyone.backend.controllers;




import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import formative.twentyone.backend.models.User;
import formative.twentyone.backend.models.Zodiac;
import formative.twentyone.backend.services.UserService;
import formative.twentyone.backend.services.ZodiacService;


@RestController
@RequestMapping("/users")
@Validated
public class UserController {

    @Autowired
    UserService service;

    @Autowired ZodiacService zodiacService;


    @GetMapping
    public Iterable<User> getAll() {
        return service.getAll();
    }

    // @PostMapping
    // public User save(@RequestBody User user) {
    //     return service.save(user);
    // }

    @PostMapping
    public User save(@RequestBody User user) {
        Zodiac zod = zodiacService.getOneByDates(user.getBirthDate().withYear(2024)).orElseThrow();
        user.setZodiac(zod);
        return service.save(user);
    }


    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public User updateById(@PathVariable int id, @RequestBody User user) {
        return service.updateById(id, user);
    }

}