package formative.twentyone.backend.repositories;


import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.twentyone.backend.models.Zodiac;




public interface ZodiacRepository extends CrudRepository<Zodiac, Integer> {

    @Query(value ="SELECT * FROM zodiac", nativeQuery = true)
    List<Zodiac> findAll();

    @Query(value = "SELECT * FROM zodiac WHERE start_date <= ?1 AND end_date >= ?1 LIMIT 1", nativeQuery = true)
    Optional<Zodiac> findByDates(LocalDate date);

    @Query(value = "SELECT * FROM zodiac LIMIT 1", nativeQuery = true)
    Optional<Zodiac> findOne();


}

// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Aries', '2024-03-21', '2024-04-19');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Taurus', '2024-04-20', '2024-05-20');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Gemini', '2024-05-21', '2024-06-20');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Cancer', '2024-06-21', '2024-07-22');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Leo', '2024-07-23', '2024-08-22');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Virgo', '2024-08-23', '2024-09-22');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Libra', '2024-09-23', '2024-10-22');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Scorpio', '2024-10-23', '2024-11-21');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Sagittarius', '2024-11-22', '2024-12-21');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Capricorn', '2024-12-22', '2025-01-19');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Aquarius', '2025-01-20', '2025-02-18');
// INSERT INTO zodiac (name, start_date, end_date) VALUES ('Pisces', '2025-02-19', '2025-03-20');

