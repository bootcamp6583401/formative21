package formative.twentyone.backend.services;



import java.util.List;


import formative.twentyone.backend.models.User;



public interface UserService {
    List<User> getAll();

    User getById(int id);

    User save(User obj);

    void deleteById(int id);

    User updateById(int id, User brand);


}
