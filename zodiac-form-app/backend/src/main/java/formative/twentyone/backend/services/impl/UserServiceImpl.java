package formative.twentyone.backend.services.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.twentyone.backend.models.User;
import formative.twentyone.backend.repositories.UserRepository;
import formative.twentyone.backend.services.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repo;

    public List<User> getAll() {
        return repo.findAll();
    }

    public User save(User comment) {
        return repo.save(comment);
    }

    public User getById(int id) {
        Optional<User> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public User updateById(int id, User comment) {

        comment.setId(id);
        return repo.save(comment);

    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }


}