package formative.twentyone.backend.models;



import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    @NotNull(message = "Name cannot be empty")
    private String name;


    @Getter
    @Setter
    @NotNull(message = "Birth date cannot be empty")
    private LocalDate birthDate;

    @ManyToOne
    @JoinColumn(name = "zodiac_id", referencedColumnName = "id")
    @JsonBackReference
    @Getter
    @Setter
    private Zodiac zodiac;

    public Zodiac getUserZodiac() {
        if(this.getZodiac() == null ) return null;
        this.getZodiac().setUsers(null);
        return this.getZodiac();
    }


}

