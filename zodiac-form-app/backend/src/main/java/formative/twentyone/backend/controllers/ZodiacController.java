package formative.twentyone.backend.controllers;






import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import formative.twentyone.backend.models.Zodiac;
import formative.twentyone.backend.services.ZodiacService;


@RestController
@RequestMapping("/zodiacs")
@Validated
public class ZodiacController {

    @Autowired
    ZodiacService service;



    @GetMapping
    public Iterable<Zodiac> getAll() {
        return service.getAll();
    }


}