import { FormEventHandler, useEffect, useState } from "react";
import "./App.css";
import { format, setDate } from "date-fns";
import getZodiacs, { Zodiac } from "./services/getZodiacs";
import ZodiacComponent from "./components/Zodiac";

type User = {
  id: number;
  name: string;
  birthDate: string;
  userZodiac: Zodiac;
};

function App() {
  const [zodiacs, setZodiacs] = useState<Zodiac[]>([]);

  const [name, setName] = useState("");

  const [birthDate, setBirthDate] = useState(format(new Date(), "yyyy-MM-dd"));

  const [selectedZodId, setSelectedZodId] = useState<null | number>(null);
  useEffect(() => {
    (async () => {
      /* v8 ignore next 2 */
      const zodiacs = await getZodiacs();
      setZodiacs(zodiacs);
    })();
  }, []);

  const handleSubmit: FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault();
    const res = await fetch("http://localhost:8080/users", {
      method: "POST",
      body: JSON.stringify({ name, birthDate }),
      headers: {
        "Content-Type": "application/json",
      },
    });

    const user: User = await res.json();

    setSelectedZodId(user.userZodiac.id);
  };

  return (
    <div className="grid grid-cols-2 gap-2">
      <div className="mb-8 h-full flex flex-col justify-center items-center p-4 rounded-md border border-black">
        <form className="flex flex-col gap-2 " onSubmit={handleSubmit}>
          <input
            placeholder="Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <input
            type="date"
            value={birthDate}
            onChange={(e) => setBirthDate(e.target.value)}
          />
          <button
            type="submit"
            className="bg-black rounded-md text-white px-4 py-2"
          >
            Submit
          </button>
        </form>
      </div>
      <ZodiacComponent selectedZodId={selectedZodId} zodiacs={zodiacs} />
    </div>
  );
}

export default App;
