import React from "react";
import { Zodiac } from "../services/getZodiacs";

export default function Zodiac({
  zodiacs,
  selectedZodId,
}: {
  zodiacs: Zodiac[];
  selectedZodId: null | number;
}) {
  return (
    <div className="grid grid-cols-12 bg-black rounded-md p-4 gap-4">
      {zodiacs.map((zod) => (
        <div
          key={zod.id}
          className={`col-span-3 rounded-md py-8 px-16 ${
            selectedZodId === zod.id ? "bg-green-300" : "bg-white"
          } flex flex-col justify-center items-center  `}
        >
          <div>{zod.name}</div>
        </div>
      ))}
    </div>
  );
}
