import { expect, test, afterEach, vi, spy } from "vitest";
import getZodiacs from "./getZodiacs";

// afterEach(() => {
//   vi.restoreAllMocks();
// });

// test("get zodiacs", async () => {
//   const spy = vi.spyOn(getZodiacs);
//   expect(spy.getMockName()).toEqual("getZodiacs");

//   expect(getZodiacs).toEqual({ test: 1 });

//   expect(spy).toHaveBeenCalledTimes(1);
// });

test("bro", () => {
  expect(1).toEqual(1);
});

global.fetch = vi.fn();

function createFetchResponse(data) {
  return { json: () => new Promise((resolve) => resolve(data)) };
}

test("mocked getZodiac", async () => {
  const res = [
    { id: 1, name: "name", startDate: "2024-09-09", endDate: "2024-09-09" },
  ];

  fetch.mockResolvedValue(createFetchResponse(res));

  const zodiacs = await getZodiacs();

  expect(fetch).toHaveBeenCalledWith("http://localhost:8080/zodiacs");

  expect(zodiacs).toStrictEqual(res);
});
