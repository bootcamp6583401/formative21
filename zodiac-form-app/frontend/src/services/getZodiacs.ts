export type Zodiac = {
  id: number;
  name: string;
  startDate: string;
  endDate: string;
};

export default async function getZodiacs(): Promise<Zodiac[]> {
  const res = await fetch("http://localhost:8080/zodiacs");

  const zodiacs: Zodiac[] = await res.json();

  return zodiacs;
}
