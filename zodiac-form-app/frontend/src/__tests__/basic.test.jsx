import renderer from "react-test-renderer";
import { test, expect, vi } from "vitest";
import App from "../App";
import Zodiac from "../components/Zodiac";

function toJson(component) {
  const result = component.toJSON();
  expect(result).toBeDefined();
  expect(result).not.toBeInstanceOf(Array);
  return result;
}

// global.getZodiacs = vi.fn();
// function createFetchResponse(data) {
//   return { json: () => new Promise((resolve) => resolve(data)) };
// }

test("Zodiac", () => {
  const component = renderer.create(
    <Zodiac
      selectedZodId={null}
      zodiacs={[
        { id: 1, name: "test", startDate: "2023-01-01", endDate: "2023-04-01" },
      ]}
    />
  );
  let tree = toJson(component);
});

test("App", () => {
  const res = [
    { id: 1, name: "name", startDate: "2024-09-09", endDate: "2024-09-09" },
  ];

  const component = renderer.create(<App />);
  let tree = toJson(component);
  expect(tree).toMatchSnapshot();
});
